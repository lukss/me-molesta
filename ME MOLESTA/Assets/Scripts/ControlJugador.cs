using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UIElements;
using UnityEngine.UIElements.Experimental;
using UnityEditor.UI;
using UnityEngine.EventSystems;

public class ControlJugador : MonoBehaviour
{
    public static float rapidezDesplazamiento = 7f;
    public Rigidbody rb;
    public float rotationV = 180f;
    public float salto;
    private bool Piso = true;
    public int maxSaltos = 2;
    public int saltoActual = 0;
    public int velRot = 3;
    [SerializeField] float time = 1f;
    private Vector3 movimiento;
    private float x, y;

    private int contador = 0; 
    private Vector3 tamañoOriginal;
    public TextMeshProUGUI healthText;
    public void Start()
    {
          rapidezDesplazamiento = 7f;
    rb = GetComponentInChildren<Rigidbody>();
        tamañoOriginal = transform.localScale;
        healthText = GameObject.Find("HealthText").GetComponent<TextMeshProUGUI>();
        UpdateHealthText();

    }



    void Update()
    {

          x = Input.GetAxis("Horizontal");
          y = Input.GetAxis("Vertical");



        movimiento = new Vector3(x, 0f, y).normalized;
     
        transform.Translate(movimiento * Time.deltaTime * rapidezDesplazamiento);


        if (Input.GetButtonDown("Jump") && (Piso || maxSaltos > saltoActual))
        {
        
  

            rb.velocity = new Vector3(0f, salto, 0f * Time.deltaTime);
            rb.AddForce(Vector3.up * salto, ForceMode.Impulse);
            Piso = false;
            saltoActual++;
        }
        if (Input.GetKeyDown(KeyCode.R))
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);

        }


        UpdateHealthText();


    }
    private void OnTriggerEnter(Collider other)
    {

        if (other.gameObject.CompareTag("Enemy"))
        {

            contador++;

            float scaleReduction = contador * 0.1f;
            transform.localScale = tamañoOriginal * (1 - scaleReduction);


            if (contador >= 10)
            {

                SceneManager.LoadScene(SceneManager.GetActiveScene().name);
            }
        }
        if (other.gameObject.CompareTag("door"))
        {

            SceneManager.LoadScene("EscenaGanaste");
        }
    }

    private void UpdateHealthText()
    {
        // Actualizar el texto con el valor actual del Slider
        healthText.text = (100 - contador * 10).ToString();
    }


}

