using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovimientoHorizontal : MonoBehaviour
{
    public Transform posPresa;
    public float rangoAlerta = 3f;
    [SerializeField] bool EstaDeAlerta = false;
    public float Velocidad = 2f;
    public LayerMask CapaPresa;

    private bool moviendoDerecha = true;
    private float limiteIzquierdo;
    private float limiteDerecho;

    private AudioSource audioSource;

    void Start()
    {
        limiteIzquierdo = transform.position.x - rangoAlerta;
        limiteDerecho = transform.position.x + rangoAlerta;

        // Obtener el componente AudioSource
        audioSource = GetComponent<AudioSource>();

        // Verificar si hay un AudioSource y asignar un audio clip si no est� asignado
        if (audioSource == null)
        {
            audioSource = gameObject.AddComponent<AudioSource>();
        }
    }

    void Update()
    {
        EstaDeAlerta = Physics.CheckSphere(transform.position, rangoAlerta, CapaPresa);
        MoverIzquierdaDerecha();

        if (EstaDeAlerta)
        {
            if (!audioSource.isPlaying)
            {
                audioSource.Play();
            }
        }
        else
        {
            if (audioSource.isPlaying)
            {
                audioSource.Stop();
            }
        }
    }

    private void MoverIzquierdaDerecha()
    {
        if (moviendoDerecha)
        {
            transform.position += Vector3.right * Velocidad * Time.deltaTime;
            if (transform.position.x >= limiteDerecho)
            {
                moviendoDerecha = false;
            }
        }
        else
        {
            transform.position += Vector3.left * Velocidad * Time.deltaTime;
            if (transform.position.x <= limiteIzquierdo)
            {
                moviendoDerecha = true;
            }
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawSphere(transform.position, rangoAlerta);
    }

}
