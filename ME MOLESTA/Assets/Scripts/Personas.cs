using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Personas : MonoBehaviour
{

    public Transform posPresa;
    public float rangoAlerta = 3f;
   [SerializeField] bool EstaDeAlerta = false;
    public float Velocidad = 0f;
    public LayerMask CapaPresa;
    public GameObject AudioSonido;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        EstaDeAlerta = Physics.CheckSphere(transform.position, rangoAlerta, CapaPresa);

        if (EstaDeAlerta == true)
        {
            Velocidad += Time.deltaTime;
            transform.LookAt(new Vector3(posPresa.position.x, transform.position.y, posPresa.position.z));
            transform.position = Vector3.MoveTowards(transform.position, new Vector3(posPresa.position.x, transform.position.y, posPresa.position.z), Velocidad * Time.deltaTime);
            AudioSonido.gameObject.SetActive(true);

        }

        if (EstaDeAlerta == false)
        {
            AudioSonido.gameObject.SetActive(false);
        }
        //

    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawSphere(transform.position, rangoAlerta);

    }
    

}

